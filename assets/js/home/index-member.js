import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.min.css'
import '@fortawesome/fontawesome-free/css/all.min.css'

import App from './SuratUkurSearch'

Vue.use(Buefy, {
    defaultIconPack: 'fas'
});

new Vue({
    render: h => h(App)
}).$mount('#app-searcher');