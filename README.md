# Arsip Tanah v1.1

Dokumen ini berisi penjelasan dan panduan penggunaan aplikasi Arsip Tanah. Silahkan diperhatikan dengan seksama untuk memudahkan dalam mengoperasikan aplikasi Arsip Tanah.

Dokumen ini dibuat oleh: Muhammad Affandes <affandes@gmail.com>

Silahkan hubungi kontak tersebut untuk mendapatkan bantuan teknis mengenai aplikasi Arsip Tanah.



## Bagian 1. Tentang Arsip Tanah

Bagian ini menjelaskan secara umum tentang aplikasi Arsip Tanah.

### 1.1 Apa itu Aplikasi Arsip Tanah?

Adalah aplikasi pencatatan dan pengarsipan berkas Surat Ukur (SU) beserta bundel SU dengan tujuan memudahkan dalam mengelola dan melacak posisi berkas tersebut. Aplikasi ini dirancang khusus untuk kebutuhan pengarsipan dengan UI yang sederhana. 

### 1.2 Fitur-Fitur

Berikut fitur-fitur yang ada dalam aplikasi ini:

1. Menambahkan Bundel baru
2. Mencari Surat Ukur 
3. Menambahkan Surat Ukur Baru
4. Mengubah status Surat Ukur
5. Mengubah status Bundel
6. Melihat semua Surat Ukur di Bundel

### 1.3 Versi dan Pembaruan

Versi 1.1



## Bagian 2. Instalasi

### 2.1 Kebutuhan Instalasi

Berikut adalah kebutuhan instalasi aplikasi Arsip Tanah. Harap dipersiapkan dan diinstal dengan baik sebelum melakukan instalasi aplikasi Arsip Tanah.

1. PHP 7.3 (rekomendasi 64-bit Non-Thread Safe)
2. Git 2 
3. Composer
4. NodeJS
5. Apache Server atau NGinX Server
6. MySQL Server atau MariaDB Server
7. Mozilla Firefox atau Google Chrome

### 2.2 Instalasi Git, Composer dan NodeJS

Berikut langkah-langkah instalasi Git pada Windows 10:

1. Download installer Git pada halaman https://git-scm.com/downloads
2. Jalankan Installer tersebut dengan pengaturan standar hingga selesai.

Berikut langkah-langkah instalasi Composer pada Windows 10:

1. Download composer setup pada halaman https://getcomposer.org/download/
2. Jalankan setup tersebut dengan pengaturan standar hingga selesai.

Berikut langkah-langkah instalasi NodeJs pada Windows 10:

1. Download Windows Installer untuk NodeJs pada halaman https://nodejs.org/en/download/
2. Jalankan installer tersebut dengan pengaturan standar hingga selesai.

### 2.3 Instalasi Server Stack

Aplikasi Server Stack merupakan aplikasi terpadu yang memudahkan anda untuk instalasi server. Aplikasi ini langsung menginstal aplikasi-aplikasi yang dibutuhkan untuk membuat server sederhana, yaitu:

1. Apache Server atau NginX
2. PHP
3. MySQL atau MariaDB
4. dan beberapa aplikasi pendukung lainnya

Jika anda menginstal aplikasi Server Stack ini, anda **tidak perlu** lagi melakukan instalasi PHP, Server dan Database. Tetapi jika anda tidak melakukan instalasi Server Stack, anda harus melakukan instalasi PHP, Server dan Database secara manual.

Berikut langkah-langkah instalasi aplikasi Server Stack pada Windows 10:

1. Download salah satu aplikasi server stack yang diinginkan, kemudian jalankan dengan pengaturan standar hingga selesai.
   1. XAMPP for Windows di https://www.apachefriends.org/download.html
   2. Wampserver di http://www.wampserver.com/en/#
   3. WAMP di https://bitnami.com/stack/wamp/installer
   4. WPN-XM di https://wpn-xm.org/downloads.php

### 2.4 Instalasi PHP

Apabila anda tidak menginstal aplikasi Server Stack, anda harus menginstal PHP secara manual. Berikut langkah-langkah instalasi PHP pada Windows 10:

1. Download file **Zip** PHP versi terbaru pada halaman https://windows.php.net/download. Pilih file dengan label **x64 Non Thread Safe.**
2. Ekstrak file **Zip** tersebut di direktori yang diinginkan. Misalnya di **C:\php**
3. Lakukan konfigurasi

### 2.5 Konfigurasi Server

Berikut konfigurasi server yang dapat digunakan.

#### 2.5.1 Konfigurasi Apache Server

Berikut langkah-langkah konfigurasi Apache Server:

#### 2.5.2 Konfigurasi NginX Server

Berikut langkah-langkah konfigurasi NginX Server:

1. Buatlah sebuah file konfigurasi dengan nama **arsip.conf** dengan isi sebagai berikut:

   ```nginx
   server {
   
       charset               utf-8;
       client_max_body_size  128M;
   
       server_name arsip.bpn.localhost;
       root        www/symfony/arsip-tanah/public;
       index       index.php;
       listen      8000;                                 ## listen for ipv4
   
       access_log  www/symfony/arsip-tanah/var/log/access.log;
       error_log   www/symfony/arsip-tanah/var/log/error.log;
   
       location / {
           try_files $uri $uri/ /index.php$is_args$args;
       }
   
       error_page 404 /error;
   
       location ~* ^.+.(gif|ico|jpg|jpeg|png|flv|swf|pdf|mp3|mp4|xml|txt|js|css|zip|rar|woff2)$ {
           try_files   $uri =404;
           expires     30d;
           add_header  Vary Accept-Encoding;
       }
   
       location ~ ^/(assets|customs|images|docs)/.*\.php$ {
           deny all;
       }
   
       location ~ \.php$ {
           include fastcgi_params;
           fastcgi_param   PHP_FCGI_MAX_REQUESTS 1000;
           fastcgi_param   PHP_FCGI_CHILDREN 100;
           fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
           fastcgi_pass    php_pool;
           try_files       $uri =404;
       }
   
       location ~* /\. {
           deny all;
       }
   
   }
   ```

   Ubahlah direktori pada variabel `server_name, root, access_log dan error_log` sesuai dengan direktori yang ada pada server anda.

2. Modifikasi file hosts pada direktori C:\Windows\System32\drivers\etc pada Windows 10. Tambahkan baris kode berikut di akhir file.

   ```
   127.0.0.1 arsip.bpn.localhost
   ```

   Sesuaikan nama domain sesuai dengan variabel `server_name` pada langkah ke 1.

3. Jalankan server atau restart server.

4. Akses domain tersebut menggunakan browser diikuti dengan alamat port yang ada di variabel `listen` pada langkah 1. Misalnya `arsip.bpn.localhost:8000`

### 2.6 Konfigurasi Database

Berikut langkah-langkah konfigurasi database:

1. Masuklah ke database server kemudian buatlah sebuah database dengan nama tertentu, misalnya **arsip-tanah**. 
2. Catatlah nama database yang baru saja anda buat.

### 2.7 Instalasi Arsip Tanah

Berikut langkah-langkah untuk instalasi aplikasi Arsip Tanah:

1. Clone file aplikasi menggunakan perintah berikut:

   ```
   git clone https://bitbucket.org/bootcamptif/arsip-tanah.git
   ```

   Untuk mengakses file tersebut dibutuhkan hak akses dari grup Bootcamp TIF dan akses internet.

2. Jalankan instalasi composer:

   ```
   php composer.phar install
   ```

   Proses ini juga membutuhkan koneksi internet untuk mendownload file-file yang diperlukan.

3. Jalankan instalasi paket NPM:

   ```
   npm install
   ```

   Proses ini juga membutuhkan koneksi internet.

4. Jalankan NPM Run Build

   ```
   npm run build
   ```



## Bagian 3. Panduan Penggunaan

Bagian ini menjelaskan tentang penggunaan aplikasi Arsip Tanah.

### 3.1 Menjalankan Aplikasi Arsip Tanah

Berikut langkah-langkah menjalankan aplikasi arsip tanah.

1. Buka `arsip.bpn.localhost:8000` menggunakan browser



### 3.2 Masuk

Untuk masuk, klik tombol Masuk pada bagian kanan atas. Kemudian masukkan username dan password. Apabila username dan password benar, maka aplikasi sudah siap digunakan.

### 3.3 Menambahkan Bundel Baru

Pada bagian Beranda, klik tombol ➕ yang ada di tengah-tengah tampilan untuk menampilkan formulir Tambah Bundel.

### 3.4 Mencari Surat Ukur (SU)

Pada bagian Beranda, tulis nomor Surat Ukur yang ingin dicari kemudian tekan Enter.

### 3.5 Menambahkan Surat Ukur (SU) Baru

Cari Surat Ukur yang diinginkan, kemudian klik tombol ➕ yang ada di kotak Surat Ukur tersebut.

### 3.6 Mengubah Status Surat Ukur (SU)

Cari Surat Ukur yang diinginkan, kemudian klik tombol 🖍 (pensil) yang ada di kotak Surat Ukur tersebut.

### 3.7 Mengubah Status Bundel

Cari Surat Ukur yang diinginkan, kemudian klik tombol 📖 (buku) yang ada di kotak Surat Ukur tersebut.

### 3.8 Melihat Semua Surat Ukur (SU)

Cari Surat Ukur yang diinginkan, kemudian klik tombol **Lihat Semua Surat Ukur** yang ada di kotak Surat Ukur tersebut.