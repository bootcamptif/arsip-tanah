<?php

namespace App\DataFixtures;

use App\Entity\Kelurahan;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class KelurahanFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rows = $this->getData();

        foreach ($rows as $row) {
            $item = new Kelurahan();
            $item->setIdKecamatan($row['idBundel']);
            $item->setNama($row['nama']);
            $manager->persist($item);
        }

        $manager->flush();
    }

    private function getData() {
        return [
            [
                'idBundel' => '1',
                'nama' => 'Kuala Cenaku',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Tambak',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Pulau Gelang',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Kuala Mulya',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Tanjung Sari',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Rawa Sekip',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Rawa Asri',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Pulau Jumat',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Sukajadi',
            ],
            [
                'idBundel' => '1',
                'nama' => 'Teluk Sungkai',
            ],
        ];
    }
}
