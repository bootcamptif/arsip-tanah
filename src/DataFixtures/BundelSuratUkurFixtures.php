<?php

namespace App\DataFixtures;

use App\Entity\BundelSuratUkur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BundelSuratUkurFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rows = $this->getData();

        foreach ($rows as $row) {
            $item = new BundelSuratUkur();
            $item->setLabel($row['label']);
            $item->setRak($row['rak']);
            $item->setStatus($row['status']);
            $manager->persist($item);
        }

        $manager->flush();
    }

    private function getData() {
        return [];
    }
}
