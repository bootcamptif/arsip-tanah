<?php

namespace App\DataFixtures;

use App\Entity\Kecamatan;
use App\Entity\Kelurahan;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class KecamatanFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rows = $this->getData();

        foreach ($rows as $row) {
            $item = new Kecamatan();
            $item->setNama($row['nama']);
            $manager->persist($item);
        }

        $manager->flush();
    }

    private function getData() {
        return [
            [
                'nama' => 'Kuala Cenaku',
            ],
        ];
    }
}
