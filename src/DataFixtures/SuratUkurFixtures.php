<?php

namespace App\DataFixtures;

use App\Entity\SuratUkur;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class SuratUkurFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $rows = $this->getData();

        foreach ($rows as $row) {
            $item = new SuratUkur();
            $item->setIdBundel($row['idBundel']);
            $item->setIdKelurahan($row['idKelurahan']);
            $item->setIdKecamatan($row['idKecamatan']);
            $item->setNomor($row['nomor']);
            $item->setTahun($row['tahun']);
            $item->setStatus($row['status']);
            $manager->persist($item);
        }

        $manager->flush();
    }

    private function getData() {
        return [];
    }
}
