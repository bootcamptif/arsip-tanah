<?php

namespace App\Controller;

use App\Entity\BundelSuratUkur;
use App\Entity\Kelurahan;
use phpDocumentor\Reflection\Types\This;
use App\Entity\SuratUkur;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="api")
     */
    public function index()
    {
        return $this->render('api/index.html.twig', [
            'controller_name' => 'ApiController',
        ]);
    }

    /**
     * @Route("/api/su-by-bundel={id}", name="api-su-bundel")
     */
    public function suByBundel($id)
    {
        if( !is_numeric($id) ) {
            return $this->json([
                'status' => false,
                'message' => 'Id harus berupa angka.'
            ]);
        }

        $su = $this->getDoctrine()->getRepository(SuratUkur::class)->findAllByIdBundel($id);

        if( !$su ) {
            return $this->json([
                'status' => false,
                'message' => 'Surat ukur tidak ditemukan.'
            ]);
        } else {
            return $this->json([
                'status' => true,
                'message' => '',
                'data' => $su
            ]);
        }

    }

    /**
     * @Route("/api/su={label}", name="api-su")
     */
    public function cariSuratUkur($label)
    {
        // Asumsi label = 14-Sukajadi-2009
        $parts = explode('-', $label);

        // Cari Kelurahan
        $kelurahan = $this->getDoctrine()
            ->getRepository(Kelurahan::class)
            ->findOneByNama($parts[1]);

        if( is_null($kelurahan) ) {
            return $this->json([
                'status' => false,
                'message' => 'Kelurahan tidak ditemukan'
            ]);
        }

        // Cari Surat Ukur
        $su = $this->getDoctrine()
            ->getRepository(SuratUkur::class)
            ->findOneByNokeltah($parts[0], $kelurahan->getId(), $kelurahan->getIdKecamatan(), $parts[2]);

        if( is_null($su) ) {
            return $this->json([
                'status' => false,
                'message' => 'Surat Ukur tidak ditemukan.',
                'data' => []
            ]);
        }

        // Cari Bundel
        $bu = $this->getDoctrine()
            ->getRepository(BundelSuratUkur::class)
            ->find($su->getIdBundel());

        if( is_null($bu) ) {
            return $this->json([
                'status' => false,
                'message' => 'Bundel tidak ditemukan.',
                'data' => []
            ]);
        } else {

            try {
                return $this->json([
                    'status' => true,
                    'message' => '',
                    'data' => [
                        'su' => $su,
                        'bu' => $bu,
                        'kel' => $kelurahan
                    ]
                ]);
            } catch (ExceptionInterface $e) {
                return $this->json([
                    'status' => false,
                    'message' => 'Ada kesalahan di server kami. Silahkan hubungi Technical Support.',
                ]);
            }
        }
    }

    /**
     * @Route("/api/kelurahan", name="api-kel-index")
     */
    public function kelurahan()
    {
        return $this->json([
            'status' => true,
            'message' => '',
            'data' => $this->getDoctrine()->getRepository(Kelurahan::class)->findAll()
        ]);
    }

    /**
     * @Route("/api/kelurahan/tes", name="api-kel-tes")
     */
    public function tesKelurahan()
    {
        return $this->json([
            'status' => true,
            'message' => '',
            'data' => $this->getDoctrine()->getRepository(Kelurahan::class)->findAllByIds([1,2,3,4,5])
        ]);
    }

    private function generateLabelBundel($n1, $n2, $desa, $thn) {
        if( $n1 == '0' ) {
            return 'Campuran/'.$desa.'/'.$thn;
        } else {
            return $n1.'-'.$n2.'/'.$desa.'/'.$thn;
        }
    }

    /**
     * @Route("/api/tambah-bundel", name="api-bundel-tambah")
     */
    public function insertBundel(Request $request)
    {
        // Ambil Post
        $no1 = $request->request->get('n1');
        $no2 = $request->request->get('n2');
        $dsa = $request->request->get('d');
        $thn = $request->request->get('t');
        $rak = $request->request->get('r');

        $em = $this->getDoctrine()->getManager();

        if( empty($no2) ) {
            if( $no1 != '0' ) {
                return $this->json([
                    'status' => false,
                    'message' => 'Gunakan 0 untuk membuat bundel campuran.',
                ]);
            } else {
                $no2 = '0';
            }
        } elseif( (int)$no2 - (int)$no1 > 1000 ) {
            return $this->json([
                'status' => false,
                'message' => 'Dalam 1 bundel sebaiknya tidak lebih dari 1000 surat ukur.',
            ]);
        }

        if( $no1 != '0' && $no2 != '0' ) {

            // Cek su sudah ada di db atau tidak
            $cekSu = $this->getDoctrine()->getRepository(SuratUkur::class)->findAllByNomor(range($no1,$no2), $dsa, $thn);
            if( $cekSu ) {

                // Cek bumdel
                $bundel = $this->getDoctrine()->getRepository(BundelSuratUkur::class)->find($cekSu[0]->getIdBundel());

                return $this->json([
                    'status' => false,
                    'message' => 'SU ini sudah pernah disimpan di bundel '.( !$bundel ? '(tidak diketahui)' : $bundel->getLabel()).'.',
                ]);
            }
        }

        // Ambil data desa/kelurahan
        $desa = $this->getDoctrine()->getRepository(Kelurahan::class)->find($dsa);

        if( !$dsa ) {
            return $this->json([
                'status' => false,
                'message' => 'Nama desa tidak ditemukan.',
            ]);
        }

        // Insert Bundel
        $bundel = new BundelSuratUkur();
        $bundel->setLabel($this->generateLabelBundel($no1, $no2, $desa->getNama(), $thn));
        $bundel->setRak($rak);
        $bundel->setStatus(1);

        $em->persist($bundel);
        $em->flush();

        // Insert SU
        $idBundel = $bundel->getId();
        $idKec = $desa->getIdKecamatan();
        $batchSize = 100;

        for ($i = $no1; $i <= $no2; $i++) {
            $su = new SuratUkur();
            $su->setIdBundel($idBundel);
            $su->setNomor($i);
            $su->setIdKelurahan($dsa);
            $su->setIdKecamatan($idKec);
            $su->setTahun($thn);
            $su->setStatus(1);
            $em->persist($su);

            if( ($i % $batchSize) === 0 ) {
                $em->flush();
                $em->clear();
            }
        }

        $em->flush();

        return $this->json([
            'status' => true,
            'message' => '',
            'data' => []
        ]);
    }

    /**
     * @Route("/api/tambah-su", name="api-su-tambah")
     */
    public function insertSu(Request $request)
    {
        // Ambil Post
        $no1 = $request->request->get('n1');
        $no2 = $request->request->get('n2');
        $dsa = $request->request->get('d');
        $thn = $request->request->get('t');
        $bdl = $request->request->get('b');
        $sta = $request->request->get('s');

        $em = $this->getDoctrine()->getManager();

        if( $no2 - $no1 > 1000 ) {
            return $this->json([
                'status' => false,
                'message' => 'Dalam sekali tambah sebaiknya tidak lebih dari 1000 surat ukur.',
            ]);
        } elseif( $no2 - $no1 < 0 ) {
            return $this->json([
                'status' => false,
                'message' => 'Gunakan range dari angka kecil ke angka besar.',
            ]);
        } elseif( $no1 < 1 ) {
            return $this->json([
                'status' => false,
                'message' => 'Gunakan angka positif.',
            ]);
        }

        // Cek su sudah ada di db atau tidak
        $cekSu = $this->getDoctrine()->getRepository(SuratUkur::class)->findAllByNomor(range($no1,$no2), $dsa, $thn);
        if( $cekSu ) {

            // Cek bumdel
            $bundel = $this->getDoctrine()->getRepository(BundelSuratUkur::class)->find($cekSu[0]->getIdBundel());

            return $this->json([
                'status' => false,
                'message' => 'SU ini sudah pernah disimpan di bundel '.( !$bundel ? '(tidak diketahui)' : $bundel->getLabel()).'.',
            ]);
        }

        // Ambil data desa/kelurahan
        $desa = $this->getDoctrine()->getRepository(Kelurahan::class)->find($dsa);

        if( !$dsa ) {
            return $this->json([
                'status' => false,
                'message' => 'Nama desa tidak ditemukan.',
            ]);
        }

        // Insert SU
        $idBundel = $bdl;
        $idKec = $desa->getIdKecamatan();
        $batchSize = 100;

        for ($i = $no1; $i <= $no2; $i++) {
            $su = new SuratUkur();
            $su->setIdBundel($idBundel);
            $su->setNomor($i);
            $su->setIdKelurahan($dsa);
            $su->setIdKecamatan($idKec);
            $su->setTahun($thn);
            $su->setStatus($sta);
            $em->persist($su);

            if( ($i % $batchSize) === 0 ) {
                $em->flush();
                $em->clear();
            }
        }

        $em->flush();

        return $this->json([
            'status' => true,
            'message' => '',
            'data' => []
        ]);
    }

    /**
     * @Route("/api/ubah-", name="api-su-ubah")
     */
    public function ubahSu(Request $request)
    {
        // Ambil Post
        $id = $request->request->get('i');
        $status = $request->request->get('s');

        $em = $this->getDoctrine()->getManager();
        $su = $em->getRepository(SuratUkur::class)->find($id);

        if( !$su ) {
            return $this->json([
                'status' => false,
                'message' => 'Surat ukur tidak ditemukan.',
                'data' => []
            ]);
        }

        $su->setStatus($status);
        $em->flush();

        return $this->json([
            'status' => true,
            'message' => '',
            'data' => [
                'su' => $su
            ]
        ]);
    }

    /**
     * @Route("/api/ubah-bundel", name="api-bundel-ubah")
     */
    public function ubahBundel(Request $request)
    {
        // Ambil Post
        $id = $request->request->get('i');
        $rak = $request->request->get('r');
        $status = $request->request->get('s');
        $info = $request->request->get('f');

        $em = $this->getDoctrine()->getManager();
        $bundel = $em->getRepository(BundelSuratUkur::class)->find($id);

        if( !$bundel ) {
            return $this->json([
                'status' => false,
                'message' => 'Bundel tidak ditemukan.',
                'data' => []
            ]);
        }

        $bundel->setRak($rak);
        $bundel->setStatus($status);
        if( $status === '2' ) {
            $bundel->setInfo($info);
            $bundel->setTgl(new \DateTime('now', new \DateTimeZone('Asia/Jakarta')));
        } else {
            $bundel->setInfo('');
            $bundel->setTgl(null);
        }

        // Jika status = 0, ubah semua status surat ukur => 0
        if( $status == 0 ) {
            $su = $em->getRepository(SuratUkur::class)->findAllByIdBundel($id);
            foreach( $su as $row ) {
                $row->setStatus(0);
                $em->persist($row);
            }
        }

        $em->flush();

        return $this->json([
            'status' => true,
            'message' => '',
            'data' => [
                'bu' => $bundel
            ]
        ]);
    }

    /**
     * @Route("/api/ubah-status-su", name="api-su-ubah-status")
     */
    public function ubahStatusSu(Request $request)
    {
        // Ambil Post
        $label = $request->request->get('n');
        $status = $request->request->get('s');

        list($nomor, $desa, $tahun) = explode('/', $label, 3);

        $em = $this->getDoctrine()->getManager();
        $kel = $em->getRepository(Kelurahan::class)->findBy(['nama' => $desa]);

        if( empty($kel) ) {
            return $this->json([
                'status' => false,
                'message' => 'Kelurahan tidak ditemukan.',
                'data' => []
            ]);
        }

        $su = $em->getRepository(SuratUkur::class)->findAllByNomor([$nomor], $kel[0]->getId(), $tahun);

        if( empty($su) ) {
            return $this->json([
                'status' => false,
                'message' => 'Surat ukur tidak ditemukan.',
                'data' => []
            ]);
        }

        foreach ($su as $row) {
            $row->setStatus($status);
        }

        $em->flush();

        return $this->json([
            'status' => true,
            'message' => '',
            'data' => $su
        ]);
    }

    /**
     * @Route("/api/hapus-su", name="api-su-hapus")
     */
    public function hapusSu(Request $request)
    {
        // Ambil Post
        $label = $request->request->get('n');

        list($nomor, $desa, $tahun) = explode('/', $label, 3);

        $em = $this->getDoctrine()->getManager();
        $kel = $em->getRepository(Kelurahan::class)->findBy(['nama' => $desa]);

        if( empty($kel) ) {
            return $this->json([
                'status' => false,
                'message' => 'Kelurahan tidak ditemukan.',
                'data' => []
            ]);
        }

        $su = $em->getRepository(SuratUkur::class)->findAllByNomor([$nomor], $kel[0]->getId(), $tahun);

        if( empty($su) ) {
            return $this->json([
                'status' => false,
                'message' => 'Surat ukur tidak ditemukan.',
                'data' => []
            ]);
        }

        foreach ($su as $row) {
            $em->remove($row);
        }

        $em->flush();

        return $this->json([
            'status' => true,
            'message' => '',
            'data' => null
        ]);
    }

}
