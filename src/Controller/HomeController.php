<?php

namespace App\Controller;

use App\Entity\Kelurahan;
use App\Entity\User;
use App\Repository\KelurahanRepository;
use phpDocumentor\Reflection\Types\This;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $user = $this->getUser();

        if( !is_null($user) && in_array('ROLE_USER',$user->getRoles()) ) {

            $kelurahan = $this->getDoctrine()
                ->getRepository(Kelurahan::class)
                ->findAll();

            return $this->render('home/index-member.html.twig', [
                'maxYear' => date('Y'),
                'kelurahan' => $kelurahan
            ]);

        } else {

            return $this->render('home/index.html.twig');

        }

    }

    /**
     * @Route("/profil", name="profil")
     */
    public function profil(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();

        if( !is_null($user) && in_array('ROLE_USER',$user->getRoles()) ) {


            if( $request->isMethod('POST') ) {
                $error = '';
                $post = $request->request->get('Profil');
                if( empty($post['new']) && strlen($post['new']) < 6 ) {
                    $error = 'Password baru harus diisi minimal 6 karakter';
                } elseif( $post['new'] !== $post['ret'] ) {
                    $error = 'Ulangi password baru dengan benar!';
                } else {
                    $user->setPassword( $encoder->encodePassword($user, $post['new']) );
                    $em->flush();

                }
                if( empty($error) ) {
                    return $this->redirectToRoute('home');
                } else {
                    return $this->render('home/profil.html.twig', [
                        'error' => $error
                    ]);
                }
            }

            return $this->render('home/profil.html.twig', [
                'error' => ''
            ]);

        } else {

            return $this->createAccessDeniedException('Silahkan Masuk terlebih dahulu.');

        }

    }

    /**
     * @Route("/tentang", name="tentang")
     */
    public function tentang()
    {
        return $this->render('home/tentang.html.twig');
    }
}
