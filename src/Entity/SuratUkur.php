<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SuratUkurRepository")
 */
class SuratUkur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var integer Bundel
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $idBundel;

    /**
     * @var integer Nomor
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $nomor;

    /**
     * @var integer Kelurahan/Desa
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $idKelurahan;

    /**
     * @var integer Kecamatan
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $idKecamatan;

    /**
     * @var integer Tahun
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $tahun;

    /**
     * Status
     * 0 = tidak ada
     * 1 = tersedia
     * 2 = (tidak digunakan) XXXXX
     * 3 = rusak
     *
     * @var integer Status
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getIdBundel(): int
    {
        return $this->idBundel;
    }

    /**
     * @param int $idBundel
     */
    public function setIdBundel(int $idBundel): void
    {
        $this->idBundel = $idBundel;
    }

    /**
     * @return int
     */
    public function getNomor(): int
    {
        return $this->nomor;
    }

    /**
     * @param int $nomor
     */
    public function setNomor(int $nomor): void
    {
        $this->nomor = $nomor;
    }

    /**
     * @return int
     */
    public function getIdKelurahan(): int
    {
        return $this->idKelurahan;
    }

    /**
     * @param int $idKelurahan
     */
    public function setIdKelurahan(int $idKelurahan): void
    {
        $this->idKelurahan = $idKelurahan;
    }

    /**
     * @return int
     */
    public function getIdKecamatan(): int
    {
        return $this->idKecamatan;
    }

    /**
     * @param int $idKecamatan
     */
    public function setIdKecamatan(int $idKecamatan): void
    {
        $this->idKecamatan = $idKecamatan;
    }

    /**
     * @return int
     */
    public function getTahun(): int
    {
        return $this->tahun;
    }

    /**
     * @param int $tahun
     */
    public function setTahun(int $tahun): void
    {
        $this->tahun = $tahun;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }


}
