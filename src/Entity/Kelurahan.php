<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\KelurahanRepository")
 */
class Kelurahan
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="smallint")
     */
    private $id;

    /**
     * @var integer Kecamatan
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $idKecamatan;
    /**
     * @var string Nama
     * @ORM\Column(type="string", length=25)
     */
    private $nama;

    /**
     * @return int
     */
    public function getIdKecamatan(): int
    {
        return $this->idKecamatan;
    }

    /**
     * @param int $idKecamatan
     */
    public function setIdKecamatan(int $idKecamatan): void
    {
        $this->idKecamatan = $idKecamatan;
    }

    /**
     * @return string
     */
    public function getNama(): string
    {
        return $this->nama;
    }

    /**
     * @param string $nama
     */
    public function setNama(string $nama): void
    {
        $this->nama = $nama;
    }

    public function getId(): ?int
    {
        return $this->id;
    }
}
