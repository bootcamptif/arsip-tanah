<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BundelSuratUkurRepository")
 */
class BundelSuratUkur
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var string Label bundel
     * @ORM\Column(type="string", length=25)
     */
    private $label;

    /**
     * @var string Kode Rak
     * @ORM\Column(type="string", length=25)
     */
    private $rak;

    /**
     * Status
     * 0 = tidak ada
     * 1 = tersedia
     * 2 = dipinjam
     * 3 = rusak
     *
     * @var integer Status
     * @ORM\Column(type="smallint", options={"unsigned"=true})
     */
    private $status;

    /**
     * @var string Info
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $info;

    /**
     * @var \DateTimeInterface Tanggal
     * @ORM\Column(type="date", nullable=true)
     */
    private $tgl;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel(string $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getRak(): string
    {
        return $this->rak;
    }

    /**
     * @param string $rak
     */
    public function setRak(string $rak): void
    {
        $this->rak = $rak;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getInfo(): ?string
    {
        return $this->info;
    }

    /**
     * @param string $info
     * @return $this
     */
    public function setInfo(string $info): self
    {
        $this->info = empty($info) ? null : $info;

        return $this;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getTgl(): ?\DateTimeInterface
    {
        return $this->tgl;
    }

    /**
     * @param \DateTimeInterface|null $tgl
     * @return $this
     */
    public function setTgl(?\DateTimeInterface $tgl): self
    {
        $this->tgl = $tgl;

        return $this;
    }

}
