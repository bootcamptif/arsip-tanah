<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191031142055 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE bundel_surat_ukur (id INT UNSIGNED AUTO_INCREMENT NOT NULL, label VARCHAR(25) NOT NULL, rak VARCHAR(25) NOT NULL, status SMALLINT UNSIGNED NOT NULL, info VARCHAR(50) DEFAULT NULL, tgl DATE DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kecamatan (id INT AUTO_INCREMENT NOT NULL, nama VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE kelurahan (id SMALLINT AUTO_INCREMENT NOT NULL, id_kecamatan SMALLINT UNSIGNED NOT NULL, nama VARCHAR(25) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE surat_ukur (id INT UNSIGNED AUTO_INCREMENT NOT NULL, id_bundel SMALLINT UNSIGNED NOT NULL, nomor SMALLINT UNSIGNED NOT NULL, id_kelurahan SMALLINT UNSIGNED NOT NULL, id_kecamatan SMALLINT UNSIGNED NOT NULL, tahun SMALLINT UNSIGNED NOT NULL, status SMALLINT UNSIGNED NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT UNSIGNED AUTO_INCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(100) NOT NULL, fullname VARCHAR(45) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE bundel_surat_ukur');
        $this->addSql('DROP TABLE kecamatan');
        $this->addSql('DROP TABLE kelurahan');
        $this->addSql('DROP TABLE surat_ukur');
        $this->addSql('DROP TABLE user');
    }
}
