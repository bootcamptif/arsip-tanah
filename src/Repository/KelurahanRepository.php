<?php

namespace App\Repository;

use App\Entity\Kelurahan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerInterface;

/**
 * @method Kelurahan|null find($id, $lockMode = null, $lockVersion = null)
 * @method Kelurahan|null findOneBy(array $criteria, array $orderBy = null)
 * @method Kelurahan[]    findAll()
 * @method Kelurahan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class KelurahanRepository extends ServiceEntityRepository
{
    private $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, Kelurahan::class);
        $this->logger = $logger;
    }

    // /**
    //  * @return Kelurahan[] Returns an array of Kelurahan objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('k')
            ->andWhere('k.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('k.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    public function findOneByNama($value): ?Kelurahan
    {
        try {
            return $this->createQueryBuilder('k')
                ->andWhere('k.nama LIKE :val')
                ->setParameter('val', $value)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->error('Non Unique Result Exception nih');
            return null;
        }
    }

    public function findAllByIds($ids)
    {
        $qb = $this->createQueryBuilder('k');
        return $qb->add('where', $qb->expr()->in('k.id', ':vals'))
            ->setParameter('vals', $ids)
            ->getQuery()
            ->getResult();
    }

}
