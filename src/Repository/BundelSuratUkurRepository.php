<?php

namespace App\Repository;

use App\Entity\BundelSuratUkur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method BundelSuratUkur|null find($id, $lockMode = null, $lockVersion = null)
 * @method BundelSuratUkur|null findOneBy(array $criteria, array $orderBy = null)
 * @method BundelSuratUkur[]    findAll()
 * @method BundelSuratUkur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BundelSuratUkurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BundelSuratUkur::class);
    }

    // /**
    //  * @return BundelSuratUkur[] Returns an array of BundelSuratUkur objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */


    /*public function findOneBySomeField($value): ?BundelSuratUkur
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }*/

}
