<?php

namespace App\Repository;

use App\Entity\SuratUkur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Log\LoggerInterface;

/**
 * @method SuratUkur|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuratUkur|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuratUkur[]    findAll()
 * @method SuratUkur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuratUkurRepository extends ServiceEntityRepository
{
    private $logger;

    public function __construct(ManagerRegistry $registry, LoggerInterface $logger)
    {
        parent::__construct($registry, SuratUkur::class);
        $this->logger = $logger;
    }

     /**
      * @return SuratUkur[] Returns an array of SuratUkur objects
      */

    public function findAllByIdBundel($id)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.idBundel = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult()
        ;
    }

     /**
      * @return SuratUkur[] Returns an array of SuratUkur objects
      */

    public function findAllByNomor(array $nomors, $idkel, $tahun)
    {
        $qb = $this->createQueryBuilder('s');
        return $qb->add('where', $qb->expr()->in('s.nomor',':nomors'))
            ->andWhere('s.idKelurahan = :idkel')
            ->andWhere('s.tahun = :tahun')
            ->setParameter('nomors', $nomors)
            ->setParameter('idkel', $idkel)
            ->setParameter('tahun', $tahun)
            ->getQuery()
            ->getResult()
        ;
    }



    public function findOneByNokeltah($nomor, $idKel, $idKec, $tahun): ?SuratUkur
    {
        try {
            return $this->createQueryBuilder('s')
                ->andWhere('s.nomor = :nomor')
                ->andWhere('s.idKelurahan = :kel')
                ->andWhere('s.idKecamatan = :kec')
                ->andWhere('s.tahun = :tahun')
                ->setParameter('nomor', $nomor)
                ->setParameter('kel', $idKel)
                ->setParameter('kec', $idKec)
                ->setParameter('tahun', $tahun)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            $this->logger->error('Non Unique Result Exception nih');
            return null;
        }
    }

}
